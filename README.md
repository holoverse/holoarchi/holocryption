# holoCryption

framaPad: https://annuel2.framapad.org/p/holoCryption

see also
  * backend: https://framagit.org/holosphere/holotools/holocrypt.git using finalcrypt for safe encryption
  * [security](security.md)

Self sovereign identity are the keys to all encrypted documents,
encryption operate like telepathy, in a sense you hold information in your consciousness (i.e. holoSphere) you are the sole identity to access it unless you shine light on it to make it visible from outside point of view (all information are for others hidden in your shadow. each person is a blackhole holding information "below" its horizon. The horizon can be viewed as the "public" holoring. any other dimension beyond that need to be granted  access to. 

about the cryptographic algorithms :
encryption algorithm used should be quantum-proof state-of-the-art algorithm 
(note: priv / public key exchanges are still based on RSA/Elleptic Curve algorithms )
 
(when use prime base algo EverCrypt can provide a "mathematic" proof of security : https://www.quantamagazine.org/how-the-evercrypt-library-creates-hacker-proof-cryptography-20190402/)

finalcrypt uses OTP therefore is quantum proof


Therefore, in practice  : 
      identities = private secrets and key pairs are generated over time (consciouness journey) with two adjacent keys in the time-serie.

    in conventional cryptography we protect information with public/private key pair :
          encrypted data = holoCrypt(privkey,data)
          decrypted data = holoCrypt(pubkey,encrypted-data)
    
    publickeys can be managed and distributed via a public key infrastructure ("PKI") run over IPLD
    ( note: IPLD is the backbone distributed database for not "filesystem" applications )
 
    instead of public-private keys we use OTP (one-time-pad) = 
     timed series of keys .. private/public key are then 
  
     public key = otp key @ time=t
     private key = otp key @time=t+1 (secret at the time t)
     
     keys generated over time are the journey of the consciousness unit so we always have a key pair available
     as "future" cannot be predicted form outside the holosphere (time travel assume you are within the sphere i.e. you are the authentic self identity hold the information) 
     
     OTP keys are derived from 2 keys-series source from two seed (partika,partikum) 
     and always combine keys using one next to the source (never the source itself) and one from the public dimention
     this translate to a key that is always n dimensions apart this providing a stable protection layer that doesn't erode over time.
     
     form key(t) to key(t+1) a SHA3 algorithm (crytographic hash function) = shake(160,key(t) 
     (can we have a cryptographic krysthal hash function ?)
     
     seed = life from create random data (life is an entropy source) ... seed is a sampled data from the entropy pull.
        (incarnate syntropy into entropy) 
     
     seed = position w/. holoVerse = (self position w/i all positions)
     position are on the (spiral) krysthal grid system (connected to center point : self identity, 12 Kathara grid) 
     (Peano, Hilbert, Z-curve plane filling curve coordinate system, can it be krystal?)
     
     
       image: http://www.ipfs.io/ipfs/QmQqvGv1pDpvFnYa7piREw2qoSHL5EYZdoNqRWP3XUcWiB
       
       encryption is encode information in a private dimension is not visible from public view (the "private" holoRing projection is just a point (holoAtom) in the public "holoRing".
       
       information is stored holographically in the multidimensional space i.e we access it via projection on any dimensions
       
       dim:0       ->  dim:1 -> dim: 2 -> dim:3
     data point -> data-ring -> data sphere -> data hyper-sphere
        
    public projected view = private view - 1 dimension  (frequency is the granted mechanism to access dimensions
    
    all sensitive data should be kept within the personal holosphere, therefore it can not leak out ton the "holoMemory".
    any private data that needs to be globally share is tokenized using a keyed cryptographic hash or is encoded using an homomorphic
    encryption. Homoporphic encryption is a class of algorythm where the computation can be done on the encrypted data (without the need of crypting them)  

